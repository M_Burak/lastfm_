package com.burak.lastfm

import androidx.multidex.MultiDexApplication

import com.burak.lastfm.di.component.AppComponent
import com.burak.lastfm.di.component.DaggerAppComponent
import com.burak.lastfm.di.module.NetworkModule


class BaseApp : MultiDexApplication() {
    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        this.appComponent = this.initDagger()
    }

    private fun initDagger() = DaggerAppComponent.create()

}