package com.burak.lastfm.di.component

import com.burak.lastfm.di.module.NetworkModule
import com.burak.lastfm.di.subcomponent.ListViewModelComponent
import com.burak.lastfm.di.scope.AppScope
import com.burak.lastfm.di.subcomponent.DetailViewModelComponent
import dagger.Component

@AppScope
@Component(
    modules = [
        NetworkModule::class
    ]
)
interface AppComponent {

    fun newAlbumListComponent(): ListViewModelComponent

    fun newAlbumDetailComponent(): DetailViewModelComponent
}