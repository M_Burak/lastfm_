package com.burak.lastfm.di.module

import androidx.lifecycle.ViewModel
import com.burak.lastfm.viewModel.DetailViewModel
import com.burak.lastfm.viewModel.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class DetailViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(DetailViewModel::class)
    internal abstract fun bindMyViewModel(detailViewModel: DetailViewModel): ViewModel
}