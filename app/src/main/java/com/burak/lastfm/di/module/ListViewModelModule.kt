package com.burak.lastfm.di.module

import androidx.lifecycle.ViewModel
import com.burak.lastfm.viewModel.ListViewModel
import com.burak.lastfm.viewModel.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ListViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(ListViewModel::class)
    internal abstract fun bindMyViewModel(listViewModel: ListViewModel): ViewModel
}