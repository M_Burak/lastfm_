package com.burak.lastfm.di.module

    import androidx.lifecycle.ViewModelProvider
import com.burak.lastfm.viewModel.DaggerViewModelFactory
import dagger.Binds
import dagger.Module

@Module
abstract class ViewModelFactoryModule {
    @Binds
    abstract fun bindViewModelFactory(viewModelFactory: DaggerViewModelFactory): ViewModelProvider.Factory
}