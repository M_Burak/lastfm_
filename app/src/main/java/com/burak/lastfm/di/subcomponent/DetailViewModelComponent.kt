package com.burak.lastfm.di.subcomponent

import com.burak.lastfm.di.module.DetailViewModelModule
import com.burak.lastfm.di.module.ListViewModelModule
import com.burak.lastfm.di.module.ViewModelFactoryModule
import com.burak.lastfm.di.scope.FragmentScope
import com.burak.lastfm.view.DetailFragment
import dagger.Subcomponent

@FragmentScope
@Subcomponent(
    modules = [
        ViewModelFactoryModule::class,
        DetailViewModelModule::class]
)
interface DetailViewModelComponent {
    fun inject(detailFragment: DetailFragment)
}