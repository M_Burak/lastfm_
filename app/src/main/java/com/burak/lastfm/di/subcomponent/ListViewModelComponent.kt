package com.burak.lastfm.di.subcomponent

import com.burak.lastfm.di.module.ListViewModelModule
import com.burak.lastfm.di.module.ViewModelFactoryModule
import com.burak.lastfm.di.scope.FragmentScope
import com.burak.lastfm.view.DetailFragment
import com.burak.lastfm.view.ListFragment
import dagger.Subcomponent

@FragmentScope
@Subcomponent(
    modules = [
        ViewModelFactoryModule::class,
        ListViewModelModule::class]
)
interface ListViewModelComponent{

    fun inject(listFragment: ListFragment)

}