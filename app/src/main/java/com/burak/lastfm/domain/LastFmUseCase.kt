package com.burak.lastfm.domain

import com.burak.lastfm.model.AlbumInfoResponse
import com.burak.lastfm.model.LastFmService
import com.burak.lastfm.model.SearchResponse
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class LastFmUseCase @Inject constructor(private val service: LastFmService) {
    fun searchAlbums(album: String): Single<SearchResponse> = service.searchAlbums(album)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())


    fun getAlbumInfo(artist: String?, album: String?): Single<AlbumInfoResponse> =
        service.getAlbumInfo(artist,album)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
}