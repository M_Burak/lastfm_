package com.burak.lastfm.model

import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.*

interface LastFmApi {


    @GET("2.0/")
    fun searchAlbums(@Query("method") method: String, @Query("album") album: String, @Query("api_key") key: String
                     , @Query("format") format: String): Single<SearchResponse>


    @GET("2.0/")
    fun getAlbumInfo(@Query("method") method: String,@Query("artist") artist: String?, @Query("album") album: String?, @Query("api_key") key: String
                 , @Query("format") format: String): Single<AlbumInfoResponse>
}