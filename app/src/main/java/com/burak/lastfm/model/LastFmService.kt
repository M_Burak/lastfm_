package com.burak.lastfm.model


import com.burak.lastfm.API_KEY
import com.burak.lastfm.FORMAT

import com.burak.lastfm.METHOD
import com.burak.lastfm.METHOD_2
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class LastFmService @Inject constructor(val api: LastFmApi) {

    fun searchAlbums(album: String): Single<SearchResponse> {
        return api.searchAlbums(
            METHOD, album, API_KEY, FORMAT
        )
    }


    fun getAlbumInfo(artist:String?,album: String?): Single<AlbumInfoResponse> {
        return api.getAlbumInfo(
            METHOD_2, album, artist, API_KEY, FORMAT
        )
    }
}