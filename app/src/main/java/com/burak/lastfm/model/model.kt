package com.burak.lastfm.model

import android.os.Parcel
import android.os.Parcelable
import com.fasterxml.jackson.annotation.*
import com.fasterxml.jackson.annotation.JsonProperty


@JsonPropertyOrder(
    "#text",
    "size"
)
data class Image(
    @JsonProperty("#text")
    val text: String,
    @JsonProperty("size")
    val size: String
)


@JsonPropertyOrder(
    "@attr",
    "albummatches",
    "opensearch:Query",
    "opensearch:itemsPerPage",
    "opensearch:startIndex",
    "opensearch:totalResults"
)
data class Results(
    @JsonProperty("@attr")
    val attr: Attr,
    @JsonProperty("albummatches")
    val albummatches: Albummatches,
    @JsonProperty("opensearch:Query")
    val opensearchQuery: OpensearchQuery,
    @JsonProperty("opensearch:itemsPerPage")
    val opensearchItemsPerPage: String,
    @JsonProperty("opensearch:startIndex")
    val opensearchSstartIndex: String,
    @JsonProperty("opensearch:totalResults")
    val opensearchTotalResults: String
)

data class Albummatches(
    @JsonProperty("album")
    val album: List<Album>
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class Album(
    @JsonProperty("tags")
    val tags: Toptags?,
    @JsonProperty("artist")
    val artist: String,
    @JsonProperty("image")
    val image: List<Image>,
    @JsonProperty("name")
    val name: String,
    @JsonProperty("url")
    val url: String,
    @JsonProperty("playcount")
    val playcount: String?,
    @JsonProperty("releasedate")
    val releasedate: String?,
    @JsonProperty("tracks")
    val tracks: Tracks?

) : Parcelable {
    constructor(parcel: Parcel) : this(
        TODO("tags"),
        parcel.readString(),
        TODO("image"),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        TODO("tracks")
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(artist)
        parcel.writeString(name)
        parcel.writeString(url)
        parcel.writeString(playcount)
        parcel.writeString(releasedate)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Album> {
        override fun createFromParcel(parcel: Parcel): Album {
            return Album(parcel)
        }

        override fun newArray(size: Int): Array<Album?> {
            return arrayOfNulls(size)
        }
    }
}

@JsonPropertyOrder(
    "for"
)
data class Attr(
    @JsonProperty("for")
    val _for: String
)


@JsonIgnoreProperties(ignoreUnknown = true)
data class Tag(
    @JsonProperty("name")
    val name: String,
    @JsonProperty("url")
    val url: String
)


@JsonIgnoreProperties(ignoreUnknown = true)
data class Artist(
    @JsonProperty("mbid")
    val mbid: String,
    @JsonProperty("name")
    val name: String,
    @JsonProperty("url")
    val url: String
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class Toptags(
    @JsonProperty("tag")
    val tag: List<Tag>
)


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder(
    "album"
)
data class AlbumInfoResponse(
    @JsonProperty("album")
    val album: Album
)

@JsonPropertyOrder(
    "#text",
    "role",
    "searchTerms",
    "startPage"
)
data class OpensearchQuery(
    @JsonProperty("#text")
    val text: String,
    @JsonProperty("role")
    val role: String,
    @JsonProperty("searchTerms")
    val searchTerms: String,
    @JsonProperty("startPage")
    val startPage: String
)


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder(
    "results"
)
data class SearchResponse(
    @JsonProperty("results")
    val results: Results
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class Tracks(
    @JsonProperty("track")
    val track: List<Track>
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class Track(
    @JsonProperty("album")
    val album: Album?,
    @JsonProperty("artist")
    val artist: Artist?,
    @JsonProperty("duration")
    val duration: String?,
    @JsonProperty("listeners")
    val listeners: String?,
    @JsonProperty("mbid")
    val mbid: String?,
    @JsonProperty("name")
    val name: String?,
    @JsonProperty("playcount")
    val playcount: String?,
    @JsonProperty("toptags")
    val toptags: Toptags?,
    @JsonProperty("url")
    val url: String?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readParcelable(Album::class.java.classLoader),
        TODO("artist"),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        TODO("toptags"),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(album, flags)
        parcel.writeString(duration)
        parcel.writeString(listeners)
        parcel.writeString(mbid)
        parcel.writeString(name)
        parcel.writeString(playcount)
        parcel.writeString(url)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Track> {
        override fun createFromParcel(parcel: Parcel): Track {
            return Track(parcel)
        }

        override fun newArray(size: Int): Array<Track?> {
            return arrayOfNulls(size)
        }
    }
}



