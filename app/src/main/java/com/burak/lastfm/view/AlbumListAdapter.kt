package com.burak.lastfm.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.burak.lastfm.R
import com.burak.lastfm.model.Album
import com.burak.lastfm.util.getProgressDrawable
import com.burak.lastfm.util.loadImage
import kotlinx.android.synthetic.main.item_album.view.*

class AlbumListAdapter(var albums: ArrayList<Album>) :
    RecyclerView.Adapter<AlbumListAdapter.AlbumViewHolder>() {
    fun updateAlbumList(newAlbums: List<Album>) {
        albums.clear()
        albums.addAll(newAlbums)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = AlbumViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_album, parent, false)
    )

    override fun getItemCount() = albums.size

    override fun onBindViewHolder(holder: AlbumViewHolder, position: Int) {
        albums.get(position).let { holder.bind(it) }
        holder.itemView.albumLayout.setOnClickListener {

            val action = albums[position].let { it1 -> ListFragmentDirections.goToDetail(it1) }
            Navigation.findNavController(holder.itemView).navigate(action)
        }
    }

    class AlbumViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val albumName = view.albumName
        private val artistName = view.tvArtistName
        private val albumPhoto = view.ivAlbum

        private val progressDrawable = getProgressDrawable(view.context)

        fun bind(album: Album) {
            albumName.text = album.name
            artistName.text = album.artist
            albumPhoto.loadImage(album.image[2].text, progressDrawable)
        }
    }
}