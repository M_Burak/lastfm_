package com.burak.lastfm.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.burak.lastfm.R
import com.burak.lastfm.model.Track
import kotlinx.android.synthetic.main.item_album_tracks.view.*

class AlbumTracksAdapter(var tracks:ArrayList<Track>): RecyclerView.Adapter<AlbumTracksAdapter.AlbumTrackViewHolder>() {

    fun updateTrackList(newTracks : List<Track>){
        tracks.clear()
        tracks.addAll(newTracks)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlbumTrackViewHolder {
        return AlbumTrackViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_album_tracks,parent,false)
        )
    }

    override fun getItemCount(): Int {
        return tracks.size
    }

    override fun onBindViewHolder(holder: AlbumTrackViewHolder, position: Int) {
        tracks[position].let { holder.bind(it,(position+1).toString()) }
    }

    class AlbumTrackViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val trackName = view.tvTrackName
        private val trackDuration = view.tvDuration
        private val trackSira = view.tvSira
        fun bind(track: Track,position: String) {
            trackName.text = track.name
            trackDuration.text = track.duration?.let { getDuration(it) }
            trackSira.text = position

        }

        fun getDuration(duration : String): String {
            var time = duration.toInt()
            if(time > 59)
            {
                var mins = time/60
                var secs = time-(mins*60)
                var secsInString =secs.toString()
                if(secs>0){
                    if(secs<10)
                    {
                        secsInString = ("0$secs")
                    }
                    return "$mins.$secsInString"
                }
                else
                    return "$mins"
            }else
            {
                return duration +"s"
            }
        }
    }
}