package com.burak.lastfm.view

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.palette.graphics.Palette
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition

import com.burak.lastfm.BaseApp
import com.burak.lastfm.R
import com.burak.lastfm.model.Album
import com.burak.lastfm.util.getProgressDrawable
import com.burak.lastfm.util.loadImage
import com.burak.lastfm.viewModel.DaggerViewModelFactory
import com.burak.lastfm.viewModel.DetailViewModel
import kotlinx.android.synthetic.main.fragment_detail.*
import java.text.DecimalFormat

import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 */
class DetailFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: DaggerViewModelFactory

    private lateinit var detailViewModel: DetailViewModel
    private var albumInfo: Album? = null
    private val trackListAdapter = AlbumTracksAdapter(arrayListOf())

    private val albumListDataObserver = Observer<Album> { it ->
        it?.let {
            it.playcount?.let {
                var plays= it.toDouble()
                val df = DecimalFormat("#,###.##")
                val finalCount = df.format(plays).toString()
                tvPlaycount.text = getString(R.string.playCountString)+" "+ finalCount
            }
            tvSinger.text = it.artist
            tvAlbumName.text = it.name
            val progressDrawable = getProgressDrawable(ivAlbum.context)
            ivAlbum.loadImage(it?.image[2].text, progressDrawable)
            setupBGColor(it?.image[2].text)
            it.tracks?.track?.let { tracks->
                trackListAdapter.updateTrackList(tracks)
            }
        }
    }

    private val loadingLiveDataObserver = Observer<Boolean> { isLoading ->
        pBarDetail.visibility = if (isLoading) View.VISIBLE else View.GONE
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity).supportActionBar?.title = getString(R.string.lastFM)
        arguments?.let {
            albumInfo = DetailFragmentArgs.fromBundle(it).album
        }

        rwTracks.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = trackListAdapter
        }

        (activity?.applicationContext as BaseApp).appComponent
            .newAlbumDetailComponent().inject(this)
        detailViewModel = ViewModelProviders.of(this, viewModelFactory)[DetailViewModel::class.java]

        observeAlbumDetail()
        detailViewModel.getAlbumInfo(albumInfo?.name, albumInfo?.artist)
    }

     private fun observeAlbumDetail() {
        detailViewModel.albumDetailLiveData.observe(this, albumListDataObserver)
        detailViewModel.loading.observe(this, loadingLiveDataObserver)
    }

    private fun setupBGColor(url: String) {
        Glide.with(this)
            .asBitmap()
            .load(url)
            .into(object : CustomTarget<Bitmap>() {
                override fun onLoadCleared(placeholder: Drawable?) {

                }

                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                    Palette.from(resource)
                        .generate() { palette ->
                            val intColor =
                                palette?.lightMutedSwatch?.rgb ?: palette?.darkVibrantSwatch?.rgb
                                ?: 0
                            llDetails.setBackgroundColor(intColor)
                        }
                }

            })
    }

    override fun getLayoutById() = R.layout.fragment_detail
}
