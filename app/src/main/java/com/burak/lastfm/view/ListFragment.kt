package com.burak.lastfm.view


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.burak.lastfm.BaseApp
import com.burak.lastfm.R
import com.burak.lastfm.model.Results
import com.burak.lastfm.viewModel.DaggerViewModelFactory
import com.burak.lastfm.viewModel.ListViewModel
import kotlinx.android.synthetic.main.fragment_list.*
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 */
class ListFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: DaggerViewModelFactory

    private lateinit var listViewModel: ListViewModel

    private val listAdapter = AlbumListAdapter(arrayListOf())

    private val albumListDataObserver = Observer<Results> { list ->
        list?.let {
            rwAlbumList.visibility = View.VISIBLE
            listAdapter.updateAlbumList(it.albummatches.album)
        }
    }

    private val loadingLiveDataObserver = Observer<Boolean> { isLoading ->
        pBar.visibility = if (isLoading) View.VISIBLE else View.GONE
        if (isLoading) {
            tvError.visibility = View.GONE
            rwAlbumList.visibility = View.GONE
        }

    }

    private val loadErrorLiveDataObserver = Observer<Boolean> { loadingError ->
        tvError.visibility = if (loadingError) View.VISIBLE else View.GONE
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity).supportActionBar?.title = getString(R.string.lastFM)

        (activity?.applicationContext as BaseApp).appComponent
            .newAlbumListComponent().inject(this)

        listViewModel = ViewModelProviders.of(this, viewModelFactory)[ListViewModel::class.java]

        rwAlbumList.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = listAdapter
        }

        btnSearch.setOnClickListener {
            if(etSearch.text.toString() == ""){
                showEmptySearchWarning()
            }else{
                observeAlbumList()
                listViewModel.searchAlbums(etSearch.text.toString())
                etSearch.setText("")
            }
        }
    }

     private  fun observeAlbumList() {
        listViewModel.albumsLiveData.observe(this, albumListDataObserver)
        listViewModel.loadingLiveData.observe(this, loadingLiveDataObserver)
        listViewModel.loadErrorLiveData.observe(this, loadErrorLiveDataObserver)
    }

    override fun getLayoutById() = R.layout.fragment_list

    private fun showEmptySearchWarning() {
        Toast.makeText(activity,"Search key required!",Toast.LENGTH_SHORT).show()
    }

}
