package com.burak.lastfm.viewModel

import androidx.lifecycle.MutableLiveData
import com.burak.lastfm.domain.LastFmUseCase
import com.burak.lastfm.model.Album
import com.burak.lastfm.model.AlbumInfoResponse
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

class DetailViewModel @Inject constructor(private val useCase: LastFmUseCase) : BaseViewModel() {

    val albumDetailLiveData = MutableLiveData<Album>()
    val loadError = MutableLiveData<Boolean>()
    val loading = MutableLiveData<Boolean>()

    fun getAlbumInfo(artist: String?, album: String?) {
        loading.value = true

        disposable.add(
            useCase.getAlbumInfo(artist, album).subscribeWith(object :
                DisposableSingleObserver<AlbumInfoResponse>() {
                override fun onSuccess(t: AlbumInfoResponse) {
                    albumDetailLiveData.value = t.album
                    loadError.value = false
                    loading.value = false
                }

                override fun onError(e: Throwable) {
                    loadError.value = true
                    loading.value = false
                }
            })
        )
    }
}