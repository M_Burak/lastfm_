package com.burak.lastfm.viewModel

import androidx.lifecycle.MutableLiveData
import com.burak.lastfm.domain.LastFmUseCase
import com.burak.lastfm.model.Results
import com.burak.lastfm.model.SearchResponse
import io.reactivex.observers.DisposableSingleObserver

import javax.inject.Inject

class ListViewModel @Inject constructor(private val usecase: LastFmUseCase) : BaseViewModel() {

    val albumsLiveData = MutableLiveData<Results>()
    val loadErrorLiveData = MutableLiveData<Boolean>()
    val loadingLiveData = MutableLiveData<Boolean>()
    var searchResponse: SearchResponse? = null

    fun searchAlbums(key: String) {
        loadingLiveData.value = true
        disposable.add(
            usecase.searchAlbums(key).subscribeWith(object :
                DisposableSingleObserver<SearchResponse>() {
                override fun onSuccess(t: SearchResponse) {
                    albumsLiveData.value = t.results
                    loadErrorLiveData.value = false
                    loadingLiveData.value = false
                    searchResponse = t
                }

                override fun onError(e: Throwable) {
                    loadErrorLiveData.value = true
                    loadingLiveData.value = false
                }
            })
        )


    }
}