package com.burak.lastfm

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.burak.lastfm.di.component.DaggerAppComponent
import com.burak.lastfm.di.module.NetworkModule
import com.burak.lastfm.domain.LastFmUseCase
import com.burak.lastfm.model.*
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.disposables.Disposable
import io.reactivex.internal.schedulers.ExecutorScheduler
import io.reactivex.plugins.RxJavaPlugins
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import java.util.concurrent.Executor
import java.util.concurrent.TimeUnit
import com.burak.lastfm.viewModel.DetailViewModel
import io.reactivex.Single


class DetailViewModelTest {

    @get:Rule
    var rule = InstantTaskExecutorRule()
    val useCase = Mockito.mock(LastFmUseCase::class.java)
    @InjectMocks
    var detailViewModel = DetailViewModel(useCase)

    private var albumInfoResponse: AlbumInfoResponse? = null
    private var albumInfoResponseObservable: Single<AlbumInfoResponse>? = null


    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        DaggerAppComponent.create()
    }


    @Before
    fun setUpRxSchedulers() {
        val immediate = object : Scheduler() {

            override fun scheduleDirect(run: Runnable, delay: Long, unit: TimeUnit): Disposable {
                return super.scheduleDirect(run, 0, unit)
            }

            override fun createWorker(): Scheduler.Worker {
                return ExecutorScheduler.ExecutorWorker(Executor { it.run() }, true)
            }
        }

        RxJavaPlugins.setInitIoSchedulerHandler { scheduler -> immediate }
        RxJavaPlugins.setInitComputationSchedulerHandler { scheduler -> immediate }
        RxJavaPlugins.setInitNewThreadSchedulerHandler { scheduler -> immediate }
        RxJavaPlugins.setInitSingleSchedulerHandler { scheduler -> immediate }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { scheduler -> immediate }
    }

    @Test
    fun getAlbumInfoSuccess() {


        loadAlbumInfoResponse()

        albumInfoResponseObservable = Single.just(albumInfoResponse)

        Mockito.`when`(useCase.getAlbumInfo(Mockito.anyString(), Mockito.anyString()))
            .thenReturn(albumInfoResponseObservable)
        detailViewModel.getAlbumInfo("album", "artist")

        Assert.assertEquals("Tempel", detailViewModel.albumDetailLiveData.value?.name)
        Assert.assertEquals(false, detailViewModel.loadError.value)
        Assert.assertEquals(false, detailViewModel.loading.value)

    }

    @Test
    fun getAlbumInfoFail() {
        albumInfoResponseObservable = Single.error(Throwable())
        Mockito.`when`(useCase.getAlbumInfo(Mockito.anyString(), Mockito.anyString()))
            .thenReturn(albumInfoResponseObservable)
        detailViewModel.getAlbumInfo("album", "artist")
        Assert.assertEquals(true, detailViewModel.loadError.value)
        Assert.assertEquals(false, detailViewModel.loading.value)
    }

    fun loadAlbumInfoResponse() {
        val image = Image(
            "https://lastfm.freetls.fastly.net/i/u/34s/7c97f46d57464ceccc6fd8c3a881e669.png",
            "small"
        )
        val imageList = arrayListOf(image)
        val album = Album(
            Mockito.mock(Toptags::class.java),
            "Colour Haze",
            imageList,
            "Tempel",
            "",
            "",
            "",
            Mockito.mock(Tracks::class.java)
        )
        albumInfoResponse = AlbumInfoResponse(album)
    }
}