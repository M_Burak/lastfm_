package com.burak.lastfm


import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.burak.lastfm.di.component.DaggerAppComponent
import com.burak.lastfm.domain.LastFmUseCase
import com.burak.lastfm.model.*
import com.burak.lastfm.viewModel.ListViewModel
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.disposables.Disposable
import io.reactivex.internal.schedulers.ExecutorScheduler
import io.reactivex.plugins.RxJavaPlugins
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import java.util.concurrent.Executor
import java.util.concurrent.TimeUnit

class ListViewModelTest {

    @get:Rule
    var rule = InstantTaskExecutorRule()
    val useCase = Mockito.mock(LastFmUseCase::class.java)
    @InjectMocks
    var listViewModel = ListViewModel(useCase)

    private var searchResponse: SearchResponse? = null
    private var searchResponseObservable: Single<SearchResponse>? = null


    @Before
    fun setupDagger() {
        MockitoAnnotations.initMocks(this)
        DaggerAppComponent
            .create()
    }

    @Before
    fun setUpRxSchedulers() {
        val immediate = object : Scheduler() {

            override fun scheduleDirect(run: Runnable, delay: Long, unit: TimeUnit): Disposable {
                return super.scheduleDirect(run, 0, unit)
            }

            override fun createWorker(): Scheduler.Worker {
                return ExecutorScheduler.ExecutorWorker(Executor { it.run() }, true)
            }
        }

        RxJavaPlugins.setInitIoSchedulerHandler { scheduler -> immediate }
        RxJavaPlugins.setInitComputationSchedulerHandler { scheduler -> immediate }
        RxJavaPlugins.setInitNewThreadSchedulerHandler { scheduler -> immediate }
        RxJavaPlugins.setInitSingleSchedulerHandler { scheduler -> immediate }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { scheduler -> immediate }
    }

    @Test
    fun getAlbumsSuccess() {
        loadSearchResponse()
        searchResponseObservable = Single.just(searchResponse)
        Mockito.`when`(useCase.searchAlbums(Mockito.anyString()))
            .thenReturn(searchResponseObservable)
        listViewModel.searchAlbums("album")
        Assert.assertEquals(1, listViewModel.albumsLiveData.value?.albummatches?.album?.size)
        Assert.assertEquals(false, listViewModel.loadErrorLiveData.value)
        Assert.assertEquals(false, listViewModel.loadingLiveData.value)

    }

    @Test
    fun getAlbumsFail() {
        searchResponseObservable = Single.error(Throwable())
        Mockito.`when`(useCase.searchAlbums(Mockito.anyString()))
            .thenReturn(searchResponseObservable)
        listViewModel.searchAlbums("album")
        Assert.assertEquals(true, listViewModel.loadErrorLiveData.value)
        Assert.assertEquals(false, listViewModel.loadingLiveData.value)
    }

    fun loadSearchResponse() {
        val image = Mockito.mock(Image::class.java)
        val imageList = arrayListOf(image)
        val album = Album(
            null,
            "Colour Haze",
            imageList,
            "Tempel",
            "",
            "",
            "",
            Mockito.mock(Tracks::class.java)
        )
        val albumList = arrayListOf(album)
        val albumMathces = Albummatches(albumList)
        val results = Results(
            Mockito.mock(Attr::class.java),
            albumMathces,
            Mockito.mock(OpensearchQuery::class.java),
            "",
            "",
            ""
        )
        searchResponse = SearchResponse(results)
    }
}